Quoco is video editor allowing you to add subtitles to videos.

[![Quoco](https://i.imgur.com/AGS2Ee8.png)](https://www.youtube.com/watch?v=mBgTCbRP6pg "Quoco")

[Download latest stable version.](https://gitlab.com/yann.alet/quoco/-/jobs/artifacts/master/download?job=build_job)

Created by [Yann Alet](https://www.linkedin.com/in/aletyann/)


Attribution
- Icon made by Freepik from www.flaticon.com