﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Quoco.Behaviors;
using Quoco.ViewModels;

namespace Quoco
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var asm = Assembly.GetExecutingAssembly();
            Title = $"Quoco - {asm.GetName().Version}";
            ViewModel = new MainWindowViewModel();
            DataContext = ViewModel;
        }

        public MainWindowViewModel ViewModel { get; set; }

        private void OnDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                ViewModel.VideoFilePath = files.FirstOrDefault();
                if (!String.IsNullOrEmpty(ViewModel.VideoFilePath))
                {
                    mediaElement.Source = new Uri(ViewModel.VideoFilePath);
                    mediaElement.MediaOpened += OnMediaOpened;
                }
            }
        }

        private void OnMediaOpened(object sender, RoutedEventArgs e)
        {
            foreach (var behavior in Interaction.GetBehaviors(mediaElement).OfType<BindTimeSpanPropertyBehavior>())
            {
                behavior.RefreshDP();
            }

            mediaElement.MediaOpened -= OnMediaOpened;
        }
    }
}
