﻿namespace Quoco.Behaviors
{
    using System;
    using System.Diagnostics;
    using System.Reflection;
    using System.Windows;
    using Microsoft.Xaml.Behaviors;


    public class BindPropertyBehavior<T> : Behavior<FrameworkElement>
    {
        public string PropertyName { get; set; }

        public bool IsTwoWays { get; set; }

        public T DP
        {
            get { return (T)GetValue(DPProperty); }
            set { SetValue(DPProperty, value); }
        }

        public static readonly DependencyProperty DPProperty = DependencyProperty.Register("DP", typeof(T), typeof(BindPropertyBehavior<T>), new PropertyMetadata(default(T), OnPropertyChanged));

        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var behavior = d as BindPropertyBehavior<T>;
                if(behavior != null && behavior.AssociatedObject != null)
                {
                    string[] properties = behavior.PropertyName.Split('.');
                    PropertyInfo pi = behavior.AssociatedObject.GetType().GetProperty(properties[0]);
                    for (int i = 1; i < properties.Length; i++)
                    {
                        pi = pi.GetType().GetProperty(properties[i]);
                    }

                    if (behavior.IsTwoWays)
                    {
                        pi.SetValue(behavior.AssociatedObject, e.NewValue);
                    }
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Failed to update property.");
            }
        }

        protected override void OnAttached()
        {
            RefreshDP();
            if (AssociatedObject != null)
            {
                AssociatedObject.DataContextChanged += AssociatedObject_DataContextChanged; ;
                
            }
            base.OnAttached();
        }

        private void AssociatedObject_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            RefreshDP();            
        }

        public void RefreshDP()
        {
            Debug.WriteLine($"Refreshing property {PropertyName}");

            if (AssociatedObject != null)
            {
                string[] properties = PropertyName.Split('.');
                object o = AssociatedObject;
                for (int i = 0; i < properties.Length; i++)
                {
                    try
                    {
                        var value = o.GetType().GetProperty(properties[i]).GetValue(o);
                        if (i == properties.Length - 1)
                        {
                            Debug.WriteLine($"Setting {PropertyName} to {value}");
                            SetValue(DPProperty, value);
                        }
                        o = value;
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                }                
            }
            else
            {
                Debug.WriteLine("Refresh aborted because AssociatedObject is null");
            }
        }
    }
}
