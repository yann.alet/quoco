﻿using System;
using Quoco.Model;

namespace Quoco.ViewModels
{
    public class SubtitleViewModel : BaseViewModel
    {
        private string _text;
        private bool _isValid = true;
        private bool isSelected;

        public SubtitleViewModel()
        {
            DeleteCommand = new RelayCommand(x => DeleteRequested?.Invoke(this, this));
        }

        public event EventHandler<SubtitleViewModel> DeleteRequested;

        public RelayCommand DeleteCommand { get; set; }
        public TimeSpan Start { get; set; }
        public TimeSpan End { get; set; }

        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                OnPropertyChanged();
            }
        }

        public bool IsValid
        {
            get => _isValid;
            set
            {
                _isValid = value;
                OnPropertyChanged();
            }
        }

        public bool IsSelected 
        {
            get => isSelected;
            set
            {
                isSelected = value;
                OnPropertyChanged();
            }
        }

        public Subtitle Subtitle
        {
            get
            {
                return new Subtitle(Text, Start, End);
            }
        }
    }
}
