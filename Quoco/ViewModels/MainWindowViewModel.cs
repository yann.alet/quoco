﻿using Quoco.Model;
using Quoco.Service;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Quoco.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        private double _convertFrom;
        private string _videoFilePath;
        private TimeSpan _videoPosition;
        private TimeSpan _videoDuration;
        private bool _isHintVisible = true;
        private SubtitleViewModel _selectedSubtitle;
        private string _logs;
        private VideoService _videoService;
        private bool _isVideoLoaded;
        private bool _isExporting;
        private string _activeSubtitle;
        private CancellationTokenSource _cancellationTokenSource;

        public MainWindowViewModel()
        {
            _videoService = new VideoService();
            _cancellationTokenSource = new CancellationTokenSource();
            _videoService.OnLogEvent += OnLogEvent;
            Subtitles = new ObservableCollection<SubtitleViewModel>();
            AddSubtitleCommand = new RelayCommand(OnAddSubtitleCommand, CanExecuteAddSubtitleCommand);
            ExportCommand = new RelayCommand(OnExportCommand, CanExecuteExportCommand);
            CancelCommand = new RelayCommand(OnCancelCommand, CanExecuteCancelCommand);
        }

        public RelayCommand AddSubtitleCommand { get; }
        public RelayCommand ExportCommand { get; }
        public RelayCommand CancelCommand { get; }
        public ObservableCollection<SubtitleViewModel> Subtitles { get; set; }

        public bool CanEdit
        {
            get
            {
                return IsVideoLoaded && !IsExporting;
            }
        }

        public string ActiveSubtitle
        {
            get => _activeSubtitle;
            set
            {
                _activeSubtitle = value;
                OnPropertyChanged();
            }
        }

        public bool IsVideoLoaded
        {
            get => _isVideoLoaded;
            set
            {
                _isVideoLoaded = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(CanEdit));
                CommandManager.InvalidateRequerySuggested();
            }
        }

        public bool IsExporting
        {
            get => _isExporting;
            set
            {
                _isExporting = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(CanEdit));
                CommandManager.InvalidateRequerySuggested();
            }
        }

        public string Logs
        {
            get => _logs;
            set
            {
                _logs = value;
                OnPropertyChanged();
            }
        }

        public bool IsHintVisible
        {
            get => _isHintVisible;
            set
            {
                _isHintVisible = value;
                OnPropertyChanged();
            }
        }

        public string VideoFilePath
        {
            get => _videoFilePath;
            set
            {
                _videoFilePath = value;
                IsHintVisible = false;
                IsVideoLoaded = !String.IsNullOrEmpty(_videoFilePath);
                OnPropertyChanged();
            }
        }

        public double ConvertFrom
        {
            get => _convertFrom;
            set
            {
                _convertFrom = value;
                OnPropertyChanged();

                ScrubTo(_convertFrom);
            }
        }

        public TimeSpan VideoPosition
        {
            get => _videoPosition;
            set
            {
                _videoPosition = value;
                OnPropertyChanged();
            }
        }

        public TimeSpan VideoDuration
        {
            get => _videoDuration;
            set
            {
                _videoDuration = value;
                OnPropertyChanged();
            }
        }

        public SubtitleViewModel SelectedSubtitle
        {
            get => _selectedSubtitle;
            set
            {
                if (_selectedSubtitle != null)
                {
                    _selectedSubtitle.PropertyChanged -= OnSubtitlePropertyChanged;
                }

                _selectedSubtitle = value;

                foreach (var subItem in Subtitles)
                {
                    subItem.IsSelected = false;
                }

                _selectedSubtitle.IsSelected = true;
                _selectedSubtitle.PropertyChanged += OnSubtitlePropertyChanged;
                OnPropertyChanged();
            }
        }

        private bool CanExecuteExportCommand(object p)
        {
            return Subtitles.Count > 0 && !IsExporting;
        }

        private bool CanExecuteAddSubtitleCommand(object p)
        {
            return IsVideoLoaded && !IsExporting;
        }

        private void OnCancelCommand(object p)
        {
            _cancellationTokenSource.Cancel();
        }

        private bool CanExecuteCancelCommand(object p)
        {
            return IsExporting;
        }

        private void OnAddSubtitleCommand(object p)
        {
            TimeSpan length = TimeSpan.FromSeconds(VideoDuration.TotalSeconds / 15);
            TimeSpan start = GetNewSubtitleStart(VideoPosition, length);
            TimeSpan end = start + length;
            var subtitle = new SubtitleViewModel { Start = start, End = end, Text = "ENTER TEXT" };
            subtitle.DeleteRequested += OnSubtitleDeleteRequested;
            Subtitles.Add(subtitle);
            RefreshActiveSubtitle();
            CommandManager.InvalidateRequerySuggested();
        }

        private TimeSpan GetNewSubtitleStart(TimeSpan defaultTime, TimeSpan length)
        {
            TimeSpan start = defaultTime;
            foreach (SubtitleViewModel sub in Subtitles)
            {
                if ((start >= sub.Start && start < sub.End) ||
                   (start + length >= sub.Start && start + length < sub.End))
                {
                    start = GetNewSubtitleStart(sub.End, length);
                }
            }

            return start;
        }

        private void OnSubtitleDeleteRequested(object sender, SubtitleViewModel e)
        {
            e.DeleteRequested -= OnSubtitleDeleteRequested;
            Subtitles.Remove(e);
            CommandManager.InvalidateRequerySuggested();
        }

        private void OnLogEvent(string logLine)
        {
            Logs = $"{logLine}{Environment.NewLine}{Logs}";
        }

        private void OnSubtitlePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RefreshActiveSubtitle();
        }

        private void OnExportCommand(object p)
        {
            Task.Run(ExportCommandAsync);
        }

        private async Task ExportCommandAsync()
        {
            Video video = new Video() { Path = VideoFilePath, Subtitles = Subtitles.Select(s => s.Subtitle).ToList() };
            IsExporting = true;
            try
            {
                await _videoService.ExportAsync(video, _cancellationTokenSource.Token).ContinueWith(x =>
                {
                    MessageBox.Show("Done!", "Quoco", MessageBoxButton.OK, MessageBoxImage.Information);
                }, _cancellationTokenSource.Token);
            }
            catch(OperationCanceledException e)
            {
                MessageBox.Show("Canceled!", "Quoco", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally
            {
                _cancellationTokenSource.Dispose();                
                _cancellationTokenSource = new CancellationTokenSource();
                IsExporting = false;
            }
        }        

        private void ScrubTo(double progress)
        {
            if (!File.Exists(VideoFilePath))
            {
                return;
            }

            double time = 0;
            if (progress > 0)
            {
                time = VideoDuration.TotalMilliseconds * (progress / 100d);
            }

            VideoPosition = TimeSpan.FromMilliseconds(time);
            RefreshActiveSubtitle();
        }

        private void RefreshActiveSubtitle()
        {
            ActiveSubtitle = String.Empty;
            foreach (var sub in Subtitles)
            {
                if (sub.Start <= VideoPosition && sub.End >= VideoPosition)
                {
                    ActiveSubtitle = sub.Text;
                    break;
                }
            }
        }
    }
}
