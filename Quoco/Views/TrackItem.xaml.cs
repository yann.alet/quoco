﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Quoco.ViewModels;

namespace Quoco.Views
{
    /// <summary>
    /// Interaction logic for TrackItem.xaml
    /// </summary>
    public partial class TrackItem : UserControl
    {
        private bool _isDraggingStart;
        private bool _isDraggingEnd;
        private bool _isDraggingItem;
        private Point _dragStartPos;
        public int _edgeWidth = 10;
        private double _beforeDragWidth;
        private Thickness _beforeDragMargin;
        private TimeSpan _beforeDragStart;
        private TimeSpan _beforeDragEnd;
        private int _beforeDragZIndex;

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(TrackItem), new PropertyMetadata(null));

        public TimeSpan Start
        {
            get { return (TimeSpan)GetValue(StartProperty); }
            set { SetValue(StartProperty, value); }
        }

        public static readonly DependencyProperty StartProperty =
            DependencyProperty.Register("Start", typeof(TimeSpan), typeof(TrackItem), new PropertyMetadata(TimeSpan.Zero));

        public TimeSpan End
        {
            get { return (TimeSpan)GetValue(EndProperty); }
            set { SetValue(EndProperty, value); }
        }

        public static readonly DependencyProperty EndProperty =
            DependencyProperty.Register("End", typeof(TimeSpan), typeof(TrackItem), new PropertyMetadata(TimeSpan.Zero));

        public bool IsValid
        {
            get { return (bool)GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); }
        }

        public static readonly DependencyProperty IsValidProperty =
            DependencyProperty.Register("IsValid", typeof(bool), typeof(TrackItem), new PropertyMetadata(true));



        public TrackItem()
        {
            InitializeComponent();
            this.MouseLeftButtonDown += TrackItem_MouseLeftButtonDown;
            this.Loaded += TrackItem_Loaded;
        }

        public TrackView TrackView { get; set; }

        private bool IsDragged
        {
            get
            {
                return _isDraggingStart || _isDraggingEnd || _isDraggingItem;
            }
        }

        private void TrackView_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            EvaluateDrop();
        }

        private void EvaluateDrop()
        {
            if (IsDragged)
            {
                if (!IsAtValidPosition())
                {
                    RevertState();
                }

                Reset();
            }
        }

        private void Reset()
        {
            _isDraggingStart = false;
            _isDraggingEnd = false;
            _isDraggingItem = false;
            //var vm = (SubtitleViewModel)DataContext;
            //vm.IsSelected = false;
            //Canvas.SetZIndex(this, _beforeDragZIndex);
            Canvas.SetZIndex((ContentPresenter)VisualTreeHelper.GetParent(this), _beforeDragZIndex);
            TrackView.MouseMove -= TrackView_MouseMove;
        }

        private void TrackView_MouseLeave(object sender, MouseEventArgs e)
        {
            EvaluateDrop();
        }

        private void TrackItem_Loaded(object sender, RoutedEventArgs e)
        {
            TrackView = GetAncestor<TrackView>(this);
            TrackView.MouseLeftButtonUp += TrackView_MouseLeftButtonUp;
            TrackView.MouseLeave += TrackView_MouseLeave;

            Margin = new Thickness(Start.TotalMilliseconds * TrackView.PixelPerMillisec, 0, 0, 0);
            Width = Math.Max(End.TotalMilliseconds - Start.TotalMilliseconds, 0) * TrackView.PixelPerMillisec;

            TrackView.SelectedItem = DataContext;
        }
        

        private T GetAncestor<T>(FrameworkElement current) where T: FrameworkElement
        {
            while (current != null && !(current is T))
            {
                current = VisualTreeHelper.GetParent(current) as FrameworkElement;
            }

            return current as T;
        }

        private void TrackItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _isDraggingStart = false;
            _isDraggingEnd = false;
            _isDraggingItem = false;

            var vm = (MainWindowViewModel)TrackView.DataContext;
            foreach (var subItem in vm.Subtitles)
            {
                subItem.IsSelected = false;
            }

            var pos = e.GetPosition(this);
            if (pos.X <= _edgeWidth)
            {
                BackupState();
                _isDraggingStart = true;
                _dragStartPos = e.GetPosition(TrackView);
                TrackView.MouseMove += TrackView_MouseMove;
                TrackView.SelectedItem = this.DataContext;
            }
            else if (pos.X >= this.ActualWidth - _edgeWidth)
            {
                BackupState();
                _isDraggingEnd = true;
                _dragStartPos = e.GetPosition(TrackView);
                TrackView.MouseMove += TrackView_MouseMove;
                TrackView.SelectedItem = this.DataContext;
            }
            else
            {
                BackupState();
                _isDraggingItem = true;                
                _dragStartPos = e.GetPosition(TrackView);
                TrackView.MouseMove += TrackView_MouseMove;
                TrackView.SelectedItem = this.DataContext;
            }
        }

        private void BackupState()
        {
            _beforeDragMargin = Margin;
            _beforeDragStart = Start;
            _beforeDragEnd = End;
            _beforeDragWidth = ActualWidth;
            _beforeDragZIndex = Canvas.GetZIndex((ContentPresenter)VisualTreeHelper.GetParent(this));

        }

        private void RevertState()
        {
            Start = _beforeDragStart;
            End = _beforeDragEnd;
            Margin = _beforeDragMargin;
            Width = _beforeDragWidth;
            var vm = (SubtitleViewModel)DataContext;
            vm.IsValid = true;
        }

        private bool IsAtValidPosition()
        {
            var vm = (MainWindowViewModel)TrackView.DataContext;
            foreach (var subItem in vm.Subtitles)
            {
                if (subItem != this.DataContext)
                {
                    if ((Start >= subItem.Start && Start <= subItem.End) ||
                        (End >= subItem.Start && End <= subItem.End) ||
                        (subItem.Start >= Start && subItem.End <= End))
                    {
                        return false;
                    }
                }
            }

            return true;
        }


        private void TrackView_MouseMove(object sender, MouseEventArgs e)
        {
            var vm = (SubtitleViewModel)DataContext;
            if (_isDraggingEnd)
            {
                var currentPos = e.GetPosition(TrackView);
                double delta = currentPos.X - _dragStartPos.X;
                double width = Math.Max(0, _beforeDragWidth + delta);
                Width = width;
                End = TimeSpan.FromMilliseconds(Start.TotalMilliseconds + ActualWidth * TrackView.MillisecPerPixel);
                Canvas.SetZIndex((ContentPresenter)VisualTreeHelper.GetParent(this), int.MaxValue);
                vm.IsValid = IsAtValidPosition();
            }
            else if(_isDraggingStart)
            {
                var currentPos = e.GetPosition(TrackView);
                double delta = currentPos.X - _dragStartPos.X;
                double left = _beforeDragMargin.Left + delta;
                left = Math.Max(0, left);
                double width = _beforeDragWidth - delta;
                width = Math.Min(End.TotalMilliseconds * TrackView.PixelPerMillisec, width);
                width = Math.Max(0, width);
                Width = width;
                Margin = new Thickness(left, 0, 0, 0);
                Start = TimeSpan.FromMilliseconds(TrackView.MillisecPerPixel * Margin.Left);
                Canvas.SetZIndex((ContentPresenter)VisualTreeHelper.GetParent(this), int.MaxValue);
                vm.IsValid = IsAtValidPosition();
            }
            else if(_isDraggingItem)
            {
                var currentPos = e.GetPosition(TrackView);
                double delta = currentPos.X - _dragStartPos.X;
                double left = Math.Max(0, _beforeDragMargin.Left + delta);
                left = Math.Min(left, TrackView.ActualWidth - ActualWidth);
                Margin = new Thickness(left, 0, 0, 0);
                Start = TimeSpan.FromMilliseconds(TrackView.MillisecPerPixel * left);
                End = TimeSpan.FromMilliseconds(Start.TotalMilliseconds + ActualWidth * TrackView.MillisecPerPixel);
                Canvas.SetZIndex((ContentPresenter) VisualTreeHelper.GetParent(this), int.MaxValue);
                vm.IsValid = IsAtValidPosition();
            }
        }
    }
}
