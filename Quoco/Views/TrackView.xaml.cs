﻿using System;
using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace Quoco.Views
{
    /// <summary>
    /// Interaction logic for TrackView.xaml
    /// </summary>
    public partial class TrackView : UserControl
    {
        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public object SelectedItem
        {
            get { return (object)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public TimeSpan VideoDuration
        {
            get { return (TimeSpan)GetValue(VideoDurationProperty); }
            set { SetValue(VideoDurationProperty, value); }
        }

        public double MillisecPerPixel 
        { 
            get
            {
                return VideoDuration.TotalMilliseconds / ActualWidth;
            }
        }

        public double PixelPerMillisec
        {
            get
            {
                return ActualWidth / VideoDuration.TotalMilliseconds;
            }
        }

        // Using a DependencyProperty as the backing store for VideoDuration.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty VideoDurationProperty =
            DependencyProperty.Register("VideoDuration", typeof(TimeSpan), typeof(TrackView), new PropertyMetadata(TimeSpan.Zero));

        // Using a DependencyProperty as the backing store for ItemsSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(TrackView), new PropertyMetadata(null));

        // Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(TrackView), new PropertyMetadata(null));

        public TrackView()
        {
            InitializeComponent();
        }
    }
}
