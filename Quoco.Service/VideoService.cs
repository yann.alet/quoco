﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Quoco.Model;

namespace Quoco.Service
{
    public class VideoService
    {
        public delegate void LogEventHandler(string logLine);
        public event LogEventHandler OnLogEvent;
        
        public string GetSubtitlesAsString(Video video)
        {
            var sb = new StringBuilder();

            for (int i = 1; i <= video.Subtitles.Count; i++)
            {
                var sub = video.Subtitles[i - 1];
                sb.AppendLine(i.ToString());
                sb.AppendLine(sub.ToString());
                if (i < video.Subtitles.Count)
                {
                    sb.AppendLine();
                }
            }

            return sb.ToString();
        }

        public void SaveSubtitles(Video video, string path)
        {
            string srt = GetSubtitlesAsString(video);
            if (!String.IsNullOrEmpty(srt))
            {
                if(File.Exists(path))
                {
                    File.Delete(path);
                }

                File.WriteAllText(path, srt);
            }
        }

        public Task ExportAsync(Video video, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                string workingDirectory = Path.GetDirectoryName(video.Path);
                string srtPath = Path.Combine(workingDirectory, $"{Path.GetFileNameWithoutExtension(video.Path)}.srt");
                if (File.Exists(srtPath))
                {
                    File.Delete(srtPath);
                }

                SaveSubtitles(video, srtPath);

                using (var ffmpeg = new FffmpegWrapper(workingDirectory))
                {
                    var psi = new ProcessStartInfo(ffmpeg.Path);
                    string outVideoFileName = $"{Path.GetFileNameWithoutExtension(video.Path)}_out{Path.GetExtension(video.Path)}";
                    string outVideoFullPath = Path.Combine(workingDirectory, outVideoFileName);
                    if (File.Exists(outVideoFullPath))
                    {
                        File.Delete(outVideoFullPath);
                    }

                    psi.WorkingDirectory = workingDirectory;
                    string args = $"-i \"{video.Path}\" -vf subtitles=\"{Path.GetFileName(srtPath)}\" \"{outVideoFullPath}\"";
                    psi.Arguments = args;
                    psi.UseShellExecute = false;
                    psi.RedirectStandardOutput = true;
                    psi.RedirectStandardError = true;
                    psi.CreateNoWindow = true;
                    var p = Process.Start(psi);
                    p.EnableRaisingEvents = true;

                    string processOutput = null;
                    while ((processOutput = p.StandardError.ReadLine()) != null)
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            p.Kill();
                            p.WaitForExit();
                            File.Delete(ffmpeg.Path);
                            File.Delete(srtPath);
                            File.Delete(outVideoFullPath);
                            OnLogEvent?.Invoke("Canceled.");
                            cancellationToken.ThrowIfCancellationRequested();
                        }

                        OnLogEvent?.Invoke(processOutput);
                    }

                    p.WaitForExit();
                    OnLogEvent?.Invoke("Done!");
                    File.Delete(srtPath);
                }
            }, cancellationToken);
        }
    }
}
