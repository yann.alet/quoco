﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Quoco.Service
{
    public class FffmpegWrapper : IDisposable
    {
        public FffmpegWrapper(string targetDirectory)
        {
            Path = System.IO.Path.Combine(targetDirectory, "ffmpeg.exe");

            if(File.Exists(Path))
            {
                File.Delete(Path);
            }

            var asm = Assembly.GetExecutingAssembly();
            Copy(asm.GetManifestResourceStream("Quoco.Service.ffmpeg.ffmpeg.exe"), Path);            
        }

        public void Copy(Stream inputStream, string outputFile)
        {
            using (var outputStream = File.OpenWrite(outputFile))
            {
                const int bufferSize = 4096;
                while (inputStream.Position < inputStream.Length)
                {
                    byte[] data = new byte[bufferSize];
                    int amountRead = inputStream.Read(data, 0, bufferSize);
                    outputStream.Write(data, 0, amountRead);
                }
                outputStream.Flush();
            }
        }

        public string Path { get; }

        public void Dispose()
        {
            var p = Process.GetProcessesByName("ffmpeg");
            if(p.Length > 0)
            {
                p[0].Kill();
            }

            if (File.Exists(Path))
            {
                File.Delete(Path);
            }
        }
    }
}
