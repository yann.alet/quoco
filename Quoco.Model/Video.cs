﻿using System.Collections.Generic;

namespace Quoco.Model
{
    public class Video
    {
        public Video()
        {
            Subtitles = new List<Subtitle>();
        }

        public string Path { get; set; }

        public List<Subtitle> Subtitles { get; set; }
    }
}
