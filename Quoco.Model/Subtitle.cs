﻿using System;

namespace Quoco.Model
{
    public class Subtitle
    {
        public Subtitle(string text, TimeSpan start, TimeSpan end)
        {
            Text = text;
            Start = start;
            End = end;
        }

        public string Text { get; }
        public TimeSpan Start { get; }
        public TimeSpan End { get; }

        public override string ToString()
        {
            return $"{Start.ToString(@"hh\:mm\:ss\,fff")} --> {End.ToString(@"hh\:mm\:ss\,fff")}{Environment.NewLine}{Text}";
        }
    }
}
